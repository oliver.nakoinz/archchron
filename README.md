# archChron

This package provides functionality for handling archaeological chronological data. The package is based on the development of the Kiel Fortification Database (KFD). Please, refer to the vignette for details: `browseVignettes(package = "archChron")`

## Installation: 

´remotes::install_gitlab("oliver.nakoinz/archchron")´ or ´devtools::install_git("git@gitlab.com:oliver.nakoinz/archchron.git")´

## Citation

@Manual{,
  title = {archChron: Handling chronological data in archaeology},
  author = {Oliver Nakoinz},
  year = {2021},
  note = {R package version 0.1.0},
}




## Todo



- Format rc dat
    - string format
    - cal format (jahresvektor)
    - chron_dist_rc  Übertragung auf chronrefObjekt
    - chron_cal_rc  kalibrieren von rc-string zu rc vector
- chron_wrrte_csv speichere Standard CronRef als csv  ???
- chron_calc_bayes
- chron_graph_triangular   triangular modelling
- improve documentation
- describe archChron in KFD handbook
- Vektoren mit attributen statt dataframe mit allem
    - attributes(x) <- list(type = "chronref", zahl = 344)
    - attributes(x)
    - attributes(x)$zahl
