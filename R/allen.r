#' allen
#'
#' calculates the relationship of two time intervals according to Allen (1983): James F. Allen: Maintaining knowledge about temporal intervals. In: Communications of the ACM. 26/11/1983. ACM Press. S. 832–843
#'
#' @param start1 int start time first time intervall
#' @param end1 int end time first time intervall
#' @param start2 int start time second time intervall
#' @param end2  int end time second time intervall
#' @param type chr return type, "chronrel" = long description, "chronrel_short" = Allen operator name, "parameter" = parameter dataframe
#'
#' @return chr text string (short or long desciption) or parameter dataframe
#' @export
#'
#' @examples
allen <- function (start1,
                   end1,
                   start2,
                   end2,
                   type = c("chronrel", "chronrel_short", "parameter")){
    descr <- c("start1 < start2",
               "start1 = start2",
               "start1 > start2",
               "end1 < end1",
               "end1 < end1",
               "end1 < end1",
               "end1 < start2",
               "end1 < start2",
               "end1 < start2",
               "end2 < start1",
               "end2 < start1",
               "end2 < start1")
    parameter <- data.frame(descr = descr,
                            value = FALSE,
                            stringsAsFactors = FALSE)
    parameter[1,2]  <- (start1 <  start2)
    parameter[2,2]  <- (start1 == start2)
    parameter[3,2]  <- (start1 >  start2)
    parameter[4,2]  <- (end1   <  end1)
    parameter[5,2]  <- (end1   == end1)
    parameter[6,2]  <- (end1   >  end1)
    parameter[7,2]  <- (end1   <  start2)
    parameter[8,2]  <- (end1   == start2)
    parameter[9,2]  <- (end1   >  start2)
    parameter[10,2] <- (end2   <  start1)
    parameter[11,2] <- (end2   == start1)
    parameter[12,2] <- (end2   >  start1)

    if(type == "chronrel"){
        if (parameter[2,2] & parameter[5,2]){result <- "d1_equals_d2"} else if (
            parameter[7,2]){result <- "d1_preceeds_d2"} else if (
                parameter[10,2]){result <- "d2_preceeds_d1"} else if (
                    parameter[8,2]){result <- "d1_meets_d2"} else if (
                        parameter[11,2]){result <- "d2_meets_d1"} else if (
                            parameter[1,2] & parameter[9,2]){result <- "d1_overlaps_d2"} else if (
                                parameter[3,2] & parameter[12,2]){result <- "d2_overlaps_d1"} else if (
                                    parameter[1,2] & parameter[6,2]){result <- "d1_contains_d2"} else if (
                                        parameter[3,2] & parameter[4,2]){result <- "d2_contains_d1"} else if (
                                            parameter[2,2] & parameter[4,2]){result <- "d1_starts_d2"} else if (
                                                parameter[2,2] & parameter[6,2]){result <- "d2_starts_d1"} else if (
                                                    parameter[5,2] & parameter[1,2]){result <- "d1_finishes_d2"} else if (
                                                        parameter[5,2] & parameter[3,2]){result <- "d2_finishes_d1"}
    }

    if(type == "chronrel_short"){
        if (parameter[2,2] & parameter[5,2]){result <- "="} else if (
            parameter[7,2]){result <- "<"} else if (
                parameter[10,2]){result <- ">"} else if (
                    parameter[8,2]){result <- "m"} else if (
                        parameter[11,2]){result <- "mi"} else if (
                            parameter[1,2] & parameter[9,2]){result <- "o"} else if (
                                parameter[3,2] & parameter[12,2]){result <- "oi"} else if (
                                    parameter[1,2] & parameter[6,2]){result <- "s"} else if (
                                        parameter[3,2] & parameter[4,2]){result <- "si"} else if (
                                            parameter[2,2] & parameter[4,2]){result <- "d"} else if (
                                                parameter[2,2] & parameter[6,2]){result <- "di"} else if (
                                                    parameter[5,2] & parameter[1,2]){result <- "f"} else if (
                                                        parameter[5,2] & parameter[3,2]){result <- "fi"}
    }

    if(type == "parameter"){return(parameter)} else {return(result)}
}



