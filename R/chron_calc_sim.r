#' chron_calc_sim
#'
#' Calculates similarities between dates with different measures accoding to BruscoM, CraditJD, SteinleyD (2021)Acomparisonof 71 binarysimilaritycoefficients:Theeffectof base rates.PLoSONE 16(4):e0247751.
#'
#' @param dat1 chron_ref object 1
#' @param dat2 chron_ref object 2
#' @param thres num threshold to turn probabilites into ocurences. Values above thres are turned to 1 else 0
#' @param fun
#'
#' @return named vector of similarities
#' @export
#'
#' @examples
chron_calc_sim <- function(dat1,
                           dat2,
                           thres = "0.5"){
    dat1[1,1] != dat2[1,1]
    if (dat1[1,1] != dat2[1,1]){
        stop("not the same chronology reference system")}
    dat1$value <- ifelse(dat1$value >= thres, 1, 0)
    dat2$value <- ifelse(dat2$value >= thres, 1, 0)
    result <- binsim(dat1$value, dat2$value)
    return(result)
}
