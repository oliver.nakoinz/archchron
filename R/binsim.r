#' binsim
#'
#'calculates binary similarities of two vectors  with different measures  according to BruscoM, CraditJD, SteinleyD (2021)Acomparisonof 71 binarysimilaritycoefficients:Theeffectof base rates.PLoSONE 16(4):e0247751.
#'
#' @param x int vector 1 (includes only only 0 and 1)
#' @param y int vector 2 (includes only only 0 and 1)
#'
#' @return num named vector of similarities
#' @export
#'
#' @examples
binsim <- function(x,y){
    a <- sum(x*y) # positive matches
    b <- sum((1-y)*x) # mismatches attribute 1
    c <- sum((1-x)*y) # mismatches attribute 2
    d <- sum((1-x)*(1-y)) # negative matches
    #n <- length(dat2$value)
    n <- a + b + c + d
    t1 <- (max(a,b) + max(c,d) + max(a,c) + max(b,d))
    t2 <- (max(a + c, b + d) + max(a + b, c + d))
    N <- n*(n - 1) / 2
    B <- a*b + c*d
    C <- a*c + b*d
    D <- a*d + b*c
    A <- N - B - C - D

    s_names <- c(
        "Dice1",
        "Dice2",
        "Jaccard",
        "SWJaccard",
        "Gleason",                  # 5
        "KulczynskiI",
        "KulczynskiII",
        "DriverandKroeber",
        "Braun-Blanquet",
        "Simpson",                  # 10
        "Sorgenfrei",
        "Mountford",
        "FagerandMcGowan",
        "SokalandSneathI",
        "McConaughey",              # 15
        "Johnson",
        "VanderMaarel",
        "ConsonniandTodeschini",
        "RussellandRao",
        "ConsonniandTodeschini",     # 20
        "SokalandMichener",
        "RogersandTanimoto",
        "SokalandSneathII",
        "SokalandSneathIII",
        "Faith",                    # 25
        "GowerandLegendre",
        "Gower",
        "AustinandColwell",
        "ConsonniandTodeschini",
        "Hamann",                   # 30
        "PeirceI",
        "PeirceII",
        "YulesQ",
        "YulesW",
        "PearsonI",                   # 35
        "PearsonII",
        "Phi",
        "Michael",
        "ColeI",
        "ColeII",                     # 40
        "Cohen",
        "MaxwellandPilliner",
        "Dennis",
        "Dispersion",
        "ConsonniandTodeschini",      # 45
        "Stiles",
        "Scott",
        "Tetrachoric",
        "Oddsratio",
        "Rand",                       # 50
        "ARI",
        "LoevingersH",
        "SokalandSneathIV",
        "SokalandSneathV",
        "RogotandGoldberg",           # 55
        "Baroni-UrbaniandBuserI",
        "PeirceIII",
        "HawkinsandDotson",
        "Tarantula",
        "HarrisandLahey",             # 60
        "ForbesI",
        "Baroni-UrbaniandBuserII",
        "Fossum",
        "ForbesII ",
        "Eyraud",                     # 65
        "Tarwid",
        "GoodmanandKruskalI",
        "Anderberg",
        "GoodmanandKruskalII ",
        "GilbertandWells",            # 70
        "ConsonniandTodeschiniII")
    result <- setNames(as.vector(1:length(s_names)), s_names)
    result[1] <- a / (a + b)
    result[2] <- a / (a + c)
    result[3] <- a / (a + b + c)
    result[4] <- 3*a / (3*a + b + c)
    result[5] <- 2*a / (2*a + b + c)
    result[6] <- a / (b + c)
    result[7] <- 0.5*(a / (a + b) + a / (a + c) )
    result[8] <- a / sqrt((a + b)*(a + c))
    result[9] <- a / max(a + b, a + c)
    result[10] <- a / min(a + b, a + c)
    result[11] <- a*a / ((a + b)*(a + c))
    result[12] <- 2*a / (a*b + a*c + 2*b*c)
    result[13] <- (a / sqrt((a + b)*(a + c)))  -  (max(a + b, a + c) / 2)
    result[14] <- a / (a + 2*b + 2*c)
    result[15] <- (a*a - b*c) / ((a + b)*(a + c))
    result[16] <- (a / (a + b)) + (a / (a + c))
    result[17] <- (2*a - b - c) / (2*a + b + c)
    result[18] <- log(1+a) / log(1 + a + b + c)
    result[19] <- a / n
    result[20] <- log(1+a) / log(1 + n)
    result[21] <- (a + d) / n
    result[22] <- (a + d) / (n + b + c)
    result[23] <- 2*(a + d) / (n + a + d)
    result[24] <- (a + d) / (b + c)
    result[25] <- (a + d/2) / n
    result[26] <- (a + d) / (a + d + (b + c)/2)
    result[27] <- (a + d) / sqrt((a + b)*(a + c)*(b + d)*(c + d))
    result[28] <- (2/pi)*sin((a + d) / n)^(-1)
    result[29] <- log(1 + a + d) / log(1 + n)
    result[30] <- (a + d - b - c) / n
    result[31] <- (a*d - b*c) / ((a + b)*(c + d))
    result[32] <- (a*d - b*c) / ((a + c)*(b + d))
    result[33] <- (a*d - b*c) / (a*d + b*c)
    result[34] <- (sqrt(a*d) - sqrt(b*c)) / (sqrt(a*d) + sqrt(b*c))
    result[35] <- n*(a*d - b*c)^2 / ((a + b)*(a + c)*(b + d)*(c + d))
    result[36] <- sqrt(result[34] / (n + result[34]))
    result[37] <- (a*d - b*c) / sqrt((a + b)*(a + c)*(b + d)*(a + d))
    result[38] <- 4*(a*d - b*c) / ((a + d)^2 + (b + c)^2)
    result[39] <- (a*d - b*c) / ((a + c)*(c + d))
    result[40] <- (a*d - b*c) / ((a + b)*(b + d))
    result[41] <- 2*(a*d - b*c) / sqrt((a + b)*(b + d) + (a + c)*(c + d))
    result[42] <- 2*(a*d - b*c) / sqrt((a + b)*(c + d) + (a + c)*(b + d))
    result[43] <- (a*d - b*c) / sqrt(n*(a + b)*(a + c))
    result[44] <- (a*d - b*c) / n^2
    result[45] <- (log(1+ a*d) - log(1 + b*c)) / log(1 + (n^2)/4)
    result[46] <- log10(n*(abs(a*d - b*c) - n/2)^2 / (a + b)*(a + c)*(b + d)*(c + d))
    result[47] <- (4*a*d - (b + c)^2) / ((2*a + b + c)*(2*d + b + c))
    result[48] <- cos(180 / (1 + sqrt((a*d)/(b*c))))
    result[49] <- (a*d) / (b*c)
    result[50] <- (A + B) / N
    result[51] <- (N*(A+D) - ((A + B)*(A + C) + (C + D)*(B + D))) / (N^2 - ((A + B)*(A + C) + (C + D) *(B + D)))
    result[52] <- NA   # Loevingers H: Pi unklar
    result[53] <- (1/4)*(a / (a + b) + a / (a + d) + d / (b + d) + d / (c + d))
    result[54] <- (a*d) / (sqrt((a + b)*(a + c)*(b + d)*(c + d)))
    result[55] <- (a / (2*a + b + c)) + (d / (2*d + b + c))
    result[56] <- (sqrt(a*d) + a) / (sqrt(a*d) + a + b + c)
    result[57] <- (a*b + a*c) / (a*b + 2*b*c + c*d)
    result[58] <- (1/2) * (a / (a + b + c)) + (d / (b + c + d))
    result[59] <- (a*(c + d)) / (a*(a + b))

    result[60] <- (a*(2*d + b + c)) / (2*(a + b + c)) + (d*(2*a + b + c)) / (2*(b + c + d))
    result[61] <- n*a / ((a + b)*(a + c))
    result[62] <- (sqrt(a*d) + a - b - c) / (sqrt(a*d) + a + b + c)
    result[63] <- (n*(a - 0.5)^2) / sqrt((a + b)*(a + c))
    result[64] <- (n*a - (a + b)*(a + c)) / (n*min(a + b, a + c) - (a + b*(a + c)))
    result[65] <- ((n^2*(n*a - (a + b)*(a + c)))) / ((a + b)*(a + c)*(b + d)*(c + d))
    result[66] <- n*a - (a + b)*(a + c) / (n*a + (a + b)*(a + c))
    result[67] <- (t1 - t2) / (2*n - t2)
    result[68] <- (t1 - t2) / 2*n
    result[69] <- (2 * min(a, d) - b - c) / (2 * min(a, d) + b + c)
    result[70] <- log(a) - log(n) - log((a + b) / n) - log((a + c) / n)
    result[71] <- (log(1+n) - log(1 + b + c)) / log(1 + n)

    return(result)
}
