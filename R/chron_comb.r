
#' chron_comb
#'
#' Combine different dates from a site into one chron_ref object
#'
#' @param dates df chron_ref list of dates (list of objects returend by chron_distr)
#' @param fun chr combining function: max, sum, mean, ocsum (sum of values over threshold)
#' @param thres num threshold for ocsum function. Values equal and over threshold are converted to 1, everything else to 0. The sum of the values is calculated then.
#' @return df chron_ref chron_distr object with combined values
#' @export
#'
#' @examples
chron_comb <- function (dates,
                        fun = c("max", "sum", "mean", "ocsum"),
                        thres = 0.5){
    result <- dates[[1]]
    result$value <- 0

    if (fun == "max"){
        dates_df <- sapply(dates,"[",,11)
        result1 <- apply(dates_df,
                         MARGIN = 1,
                         FUN    = "max")
        result$value <- result1
    }

    if (fun == "sum"){
        dates_df <- sapply(dates,"[",,11)
        result1 <- apply(dates_df,
                         MARGIN = 1,
                         FUN    = "sum")
        result$value <- result1
    }

    if (fun == "mean"){
        dates_df <- sapply(dates,"[",,11)
        result1 <- apply(dates_df,
                         MARGIN = 1,
                         FUN    = "mean")
        result$value <- result1
    }

    if (fun == "ocsum"){
        dates_df <- sapply(dates,"[",,11)
        ifelse (dates_df >= thres, 1, 0)
        #dates_df[dates_df[,] >= thres] <- 1
        #dates_df[dates_df[,] < thres] <- 0
        result1 <- apply(dates_df,
                         MARGIN = 1,
                         FUN = "sum")
        result$value <- result1
    }
    return(result)
}
