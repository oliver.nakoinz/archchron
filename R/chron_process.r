

#' chron_process
#'
#' Process a vector of sites with multiple dates per site and create lists of chron_ref objects.
#'
#' @param id num vector of site ids
#' @param chronstrings chr vector of chronology string texts for each site according to id
#' @param chronref df chron_ref with the description of chronological system descriptions
#'
#' @return list for each chronology named after the chronology system including a list of all sites with dates from this system named with site id and including a dataframe with the chronology reference object. Chronological systems without dates are NA. Sites with mixed chronologies emerge in different chronological system lists.
#' @export
#'
#' @examples
chron_process <- function (id,
                           chronstrings,
                           chronref = chron_ref){
    nnn <- length(id)
    c_systems <- unique(chron_ref$c_system)
    dates_df <- data.frame(index = 1:(20*nnn),    # index of id and chronstring vector
                           c_string = "", # individiual element from chronstring
                           #system = 0,
                           stringsAsFactors = FALSE)   # index of chronsystem in c_systems
    df_index <- 1
    for (i in 1:nnn){ # create df of single chron-strings
        chronsplit <- strsplit(chronstrings[i],
                               split = ",",
                               fixed = T)
        chronsplit <- sapply(chronsplit,
                             FUN = "trimws")
        for(j in 1:length(chronsplit)){
            dates_df$index[df_index] <- i
            dates_df$c_string[df_index] <- chronsplit[j]
            df_index <- df_index + 1
        }
    }
    dates_df <- dates_df[1:(df_index - 1),]  ## cut dates_df to the required length
    # create a list (dates_list) for each system and insert a dataframe of the dates of this system to each
    dates_list    <- vector("list", length(c_systems))

    for (i in 1:length(c_systems)){
        c_pattern <- c_systems[i]
        ind <- grepl(c_pattern, dates_df$c_string)
        df <- data.frame(index = dates_df$index[ind],
                         c_string = dates_df$c_string[ind],
                         stringsAsFactors = FALSE)
        assign(c_pattern, df)
        dates_list[[i]] <- assign(c_pattern, df)
    }

    # calculation of dates for each df in dates_list
    dates_result <- setNames(as.list(1:length(c_systems)),c_systems)
    for (i in 1:length(c_systems)){
        sites   <- unique(dates_list[[i]]$index)
        sites_n <- length(sites)
        if (sites_n == 0){
            dates_result[[i]] <-  NA
            next}
        # calculate and combine dates for sites:
        dates_reflist <- setNames(as.list(1:sites_n),id[sites])
        for (j in 1:sites_n){
            chrondates <- lapply(
                dates_list[[i]][dates_list[[i]]$index == sites[j],2],
                FUN       = "chron_distr",
                chron_ref = chronref)
            dates_reflist[[j]] <- setNames(as.data.frame(chron_comb(chrondates)),c("c_system","id","p_id","c_level","c_order","datstring" ,"name_short","start","end","duration","value"))
        }
        dates_result[[i]] <- dates_reflist
    }
    return(dates_result)
}

