#' chron_calc_prob
#'
#' Calculates max, min or sum of the probabilities of two chronology reference objects
#'
#' @param dat1 chron_ref first date
#' @param dat2 chron_ref second date
#' @param fun chr max, min or sum
#'
#' @return chron_ref date
#' @export
#'
#' @examples
chron_calc_prob <- function(dat1,
                            dat2,
                            fun = c("max", "min", "sum")){
    dat1[1,1] != dat2[1,1]
    if (dat1[1,1] != dat2[1,1]){
        stop("not the same chronology reference system")}
    dat3 <- dat1
    if (fun == "max"){
        dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = "max")} else if (fun == "min") {
            dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = "min")} else if (fun == "sum") {
                dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = "sum")
            }
    return(dat3)
}
