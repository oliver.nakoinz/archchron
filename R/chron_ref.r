#' Chronology system referenc
#'
#' This dataframe includes the description of archaeological chronology systems.
#' This file is both, a template for custom chronology system references and a collection of chronologies used for the Kiel fortification database.
#' Cvi). The outcome is the root tip angle (in degrees) at two-minute
#' increments over eight hours.
#'
#' @docType data
#'
#' @usage data(chron_ref)
#'
#' @format data.frame with 10 variables
#'
#'$ c_system  : chr chronology system
#'$ id        : int id of phase inside chronology system
#'$ p_id      : int id of superior element
#'$ c_level   : int hierarchy level
#'$ c_order   : int order inside one hierarchy level
#'$ datstring : chr chronology string
#'$ name_short: chr short name of the current phase of this row
#'$ name_long : chr long name of the current phase of this row
#'$ start     : int start time (year) of phase
#'$ end       : int end time (year) of phase
#'
#' @keywords datasets
#'
#' @references NAKOINZ, Oliver, 2012. Datierungskodierung und chronologische Inferenz - Techniken zum Umgang mit unscharfen chronologischen Informationen. Prähist. Zeitschr. 87, 189–207.
#'
#'
#' @source Kiel Fortification Project; [ArkeoGIS](https://arkeogis.org/de/)
#'
#' @examples
#' data(chron_ref)
#' chron_ref$datstring[3:10]
#'
