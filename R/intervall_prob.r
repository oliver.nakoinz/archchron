
#' intervall_prob
#'
#' calculates the probability of a date in interval x being in interval y.
#'
#' @param x1 num, start interval 1
#' @param x2 num, end   interval 1
#' @param y1 num, start interval 2
#' @param y2 num, end   interval 2
#'
#' @return num, probability
#'
#' @examples
intervall_prob <- function(x1,
                           x2,
                           y1,
                           y2){
    if ((y1 >  x2) | (y2 <  x1)) {
       result <- 0
    } else {
        result <- (min(x2, y2) -  max(x1, y1)) /  (x2 - x1)
    }
    return(result)
}
