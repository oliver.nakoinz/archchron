#' chron_distr
#'
#' Distribute Date: A chronology string is distributed to a complete entry in the chronology reference object.
#'
#' @param chron_string chr Text string containing chronology string, e. g. "fortME_Vorg>EZ>Ha>HaC_0.6"
#' @param chron_ref df chron_ref chronology description df
#' @param nc_uncertainty logical, no chronological uncertainty: It is assumed that values below 1 are due to uncertainties which are not caused by the chronological hierarchy but other factors which do not accumulate over hierarchy levels. Propabilities are not accumulated if true.
#' @param distr_timebased logical, if true the values are weighted according to the phase duration, if false all weights are equal.
#'
#' @return df dataframe similar to the database description in chron_ref_file but with only one chronology system and with a value column (with probabilities)
#' @export
#'
#' @examples
chron_distr <- function (chron_string,
                         chron_ref = chron_ref,
                         nc_uncertainty  = TRUE,
                         distr_timebased = FALSE){
    # decomposing chron_string
    chron_split <- chron_string_decompose(chron_string)
    suppressWarnings(if (is.na(chron_split)){
        return(NA)
        stop("incorrect formed chronology string")})
    if (is.na(chron_split[[3]])){chron_split[[3]] <- 1}
    c_sys   <- chron_split[[1]]
    c_str   <- chron_split[[2]]
    #c_str_v <- chron_split[[3]]
    if (is.na(chron_split[[3]])){c_val <- 1} else {
        c_val <- chron_split[[3]]}

    # preparing dataframe
    if (is.element(c_sys, unique(chron_ref$c_system)) == FALSE){
        stop(paste0("unknown chronological reference system: ", c_sys))}
    chron_ref2 <- chron_ref[chron_ref$c_system == c_sys,
                            c(1,2,3,4,5,6,7,9,10)]
    chron_ref2 <- data.frame(chron_ref2,
                             duration = 0,
                             value    = 0)
    chron_ref2$duration <- chron_ref2$end - chron_ref2$start

    # set main date
    start_index <- which(chron_ref2$name_short == c_str[1])
    if (length(start_index) == 0){
        return(chron_ref2)
        warning(paste0("unknown chronological string: ", chron_string))}
    chron_ref2$value[start_index] <- c_val

    # populate the superior elements
    i <- start_index
    if (i > 1){
        val <- c_val
        repeat{
            i <- chron_ref2$p_id[i]
            if(i == 0) {break}
            if (val < 1){
                if (nc_uncertainty == FALSE){
                    val <- sum(chron_ref2$c_level[i] == chron_ref2$c_level) * val
                } # if nc_uncertainty
                if (val > 1) {val <- 1}
            } #if val
            chron_ref2$value[i] <- val
        } # repeat
    } # if i > 1

    # populate the inverior elements (aoristics)
    val <- c_val
    sups <- chron_ref2$id[chron_ref2$p_id >= start_index]
    if (length(sups > 0)){
        for (i in sups){
            par <- chron_ref2$p_id[i]
            sibs <- chron_ref2$id[chron_ref2$p_id == chron_ref2$p_id[i]]
            if (distr_timebased == FALSE){
                chron_ref2$value[i] <- chron_ref2$value[par] / length(sibs)
            }else{ #distribute according to time
                dur   <- chron_ref2$duration[sibs]
                sdur  <- sum(dur)
                dur_i <- chron_ref2$duration[i]
                chron_ref2$value[i] <- chron_ref2$value[par] * dur_i / sdur
            } # else
        } # for i in sups
    } # if l sups>0
    chron_ref2$value <- round(chron_ref2$value,
                              digits = 3)
    return(chron_ref2)
}

