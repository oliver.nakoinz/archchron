#' chron_string_decompose
#'
#'decomposes a chronology string into its parts
#'
#' @param string chr
#'
#' @return list with parts
#' @export
#'
#' @examples
chron_string_decompose <- function(string){
    string <- sapply(string,
                     FUN = "trimws")
    chron_string_s <- strsplit(string,
                               split = "_",
                               fixed = T)
    if (length(chron_string_s[[1]]) < 2){
        return(NA)
        stop("incorrect formed chronology string")}
    c_sys <- chron_string_s[[1]][1]
    c_str <- chron_string_s[[1]][2]
    if (length(chron_string_s[[1]]) == 3) {
        c_val <- chron_string_s[[1]][3]
        c_val <- as.numeric(c_val)
    }else{
        c_val <- NA
    }
    c_str_v <- rev(strsplit(c_str,
                            split = ">",
                            fixed = T)[[1]])
    result <- list(c_system = c_sys,
                   c_string = c_str_v,
                   c_value = c_val)
    return(result)
}
