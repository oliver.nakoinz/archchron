#' chron_graph_single
#'
#' produces a plot of a singel chron_ref
#'
#' @param dat df chron_ref date
#' @param cmin start of plotted time interval
#' @param cmax end of plotted time interval
#' @param thres int if thres = 0 not considered, if thres > 0 used to turn values in occurences
#' @param main title of the graph
#'
#' @return -
#' @export
#'
#' @examples
chron_graph_single <- function(dat,
                               cmin  = - 1500,
                               cmax  = 0,
                               thres = 0,
                               main  = NULL,
                               xaxt  = TRUE){
    if(thres > 0) (dat$value <- ifelse(dat$value >= thres, 1, 0))
    dat$start[dat$start < cmin] <- cmin
    dat$start[dat$start > cmax] <- cmax
    dat$end[dat$end > cmax] <- cmax
    dat$end[dat$end < cmin] <- cmin
    dat$mean <- apply(cbind(dat$start, dat$end), MARGIN = 1, FUN = "mean")
    dat$dur <- dat$end - dat$start
    dat <- dat[dat$dur != 0,]   # remove lines out of scope
    hlevels <- max(dat$c_level)

    if(xaxt == TRUE){
        m_b  <- 2
        xaxt <- NULL} else {
            m_b  <- 0
            xaxt <- "n"
        }
    if(is.null(main)){m_t <- 0}else{m_t <- 2}
    par(mar = c(m_b, 0.25, m_t, 0.25))

    plot(NULL,
         xlim = c(cmin,
                  cmax),
         ylim = c(0,
                  hlevels),
         ylab = "",
         xlab = "",
         las = 1,
         yaxt = "n",
         xaxt = xaxt,
         main = main
    )
    for (i in 1:length(dat$id)){
        rect(dat$start[i],
             dat$c_level[i]-1,
             dat$end[i],
             dat$c_level[i],
             density = NULL,
             col =  hsv(1, 0 , 1 - dat$value[i]),
             border = "grey50")
    }
}
