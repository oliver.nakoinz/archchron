#' chron_filter_chronref
#'
#' Tests if a chronology string is coverd (val equal or higher) by chron_ref objects. Calls chron_filter_chronref_one for each element in a list of chron_ref dataframes
#'
#' @param pattern chr chronology string
#' @param object list of chron_ref dataframes
#' @param thres num probability threshold, overrided the threshold in the pattern if thres > 0. At least one threshold (in pattern or in thres) is required
#'
#' @return logical vector, T if the object covers the pattern
#' @export
#'
#' @examples
chron_filter_chronref <- function(pattern,
                                  object,
                                  thres = 0){     # overrides pattern
    result <- sapply(object,
                     FUN     = chron_filter_chronref_one,
                     pattern = pattern,
                     thres   = thres)
    return(unlist(result))
}
