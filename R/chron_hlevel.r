#' chron_hlevel
#'
#' retrive chronology hierarchy level from chronology string or number
#'
#' @param x chr/int chronology string or level number
#'
#' @return int chronology hierarchy level
#' @export
#'
#' @examples
chron_hlevel <- function(x){
    if (is.numeric(x)){return(x)} else {
        dat <- chron_string_decompose(x)
        return(length(dat[[2]]))
    }
}
