#' chron_string_list
#'
#' creates a dataframe of id and chronology strings from the chron_process output
#'
#' @param dates_list chron_process object
#' @param type chr "list" produces a list like chron_process (sublists for each chronology system). "df" produces a dataframe with id and chronology strings with one line for each site (merged chronologies)
#'
#' @return list or dataframe
#' @export
#'
#' @examples
chron_string_list <- function(dates_list,
                              type = c("list", "df")){
    c_systems <- names(dates_list)
    dates_result <- setNames(as.list(1:length(c_systems)),c_systems)
    for (i in 1:length(c_systems)){
        if (is.na(dates_list[[i]])){next}
        dates_sys <- dates_list[[i]]
        date_strings <- sapply(dates_sys,
                               FUN = "chron_string_combine")
        dates_result[[i]] <- data.frame(id = names(date_strings),
                                        dates = date_strings,
                                        stringsAsFactors = FALSE)
    }
    if (type == "list"){
        return(dates_result)
    }else{
        df <- do.call(rbind, dates_result)
        sites <- unique(df[,1])
        df2 <- data.frame(id = sites,
                          dates = "date_strings",
                          stringsAsFactors = FALSE)
        for(i in 1:length(sites)){
            df2[i,2] <- paste(df[df2[i,1] == df2[,1],2],
                              sep = ", ")
        }
        return(df2)
    }
}

