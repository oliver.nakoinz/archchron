#' chron_string_combine
#'
#' Create complete chronology string for one date (one chron_ref object)
#'
#' @param chron_ref chron_ref one chron_ref object
#'
#' @return chr text string with all comma separeted chron strings
#' @export
#'
#' @examples
chron_string_combine <- function (chron_ref){
    chronstrings <- apply(chron_ref,
                          MARGIN = 1,
                          FUN = "chron_string_create")
    chronstrings[chron_ref$val > 0]
    chronstrings <- paste(chronstrings[chron_ref$val > 0],
                          collapse = ", ")
    return(chronstrings)
}
