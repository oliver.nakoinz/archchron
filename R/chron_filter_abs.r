#' chron_filter_abs
#'
#' Applies chron_filter_absone to a list of dates. Tests, if an absolute date (y) with an uncertainty (+-a) has a probability of at least p to be included in the invervall of dat.
#'
#' @param dat  list of chron_ref dataframes
#' @param y num, year to test for
#' @param a num,  uncertainty (+-years)
#' @param p num, minimal probability
#'
#' @return log, vector of true or false
#' @export
#'
#' @examples
chron_filter_abs <- function(dat,
                             y,
                             a = 50,
                             p = 0.7) {
    result <- sapply(dat,
                     FUN = archChron::chron_filter_abs_one,
                     y   = y,
                     a   = a,
                     p   = p)
    return(unlist(result))
}
