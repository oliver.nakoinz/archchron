#' chron_filter_chronref_one
#'
#' tests if a chronology string is coverd (val equal or higher) by a chron_ref object
#'
#' @param pattern chr chronology string
#' @param object df chron_ref
#' @param thres num probability threshold, overrided the threshold in the pattern if thres > 0. At least one threshold (in pattern or in thres) is required
#'
#' @return logical T if the object covers the pattern
#' @export
#'
#' @examples
chron_filter_chronref_one <- function(pattern,
                                      object,
                                      thres = 0){
    pat <- chron_string_decompose(pattern)
    pat_sys    <- pat[[1]]
    pat_level  <- length(pat[[2]])
    pat_search <- pat[[2]][1]
    pat_prob   <- pat[[3]]
    if (pat_sys != object[1,1]){
        return(NA)
        stop("Not the same chronology reference system!")}
    if (is.na(pat_prob)){pat_prob <- thres}
    if (thres == 0){thres <- pat_prob}
    if (sum(object$value >= thres) == 0){
        return(FALSE)  # return(NA)
        warning("all probabilities are below threshold")} else if (max(object$c_level[object$value >= thres]) < pat_level){
        return(FALSE)  # return(NA)
        warning("the tested date does not reach the required chronology hierarchy level")} else {
            result <- object$value[object$name_short == pat_search] >= pat_prob
            return(result)}
}
