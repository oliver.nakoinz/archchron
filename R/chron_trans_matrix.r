#' chron_trans_matrix
#'
#' @param csys_from chr, name of chronological reference system to transform
#' @param csys_to   chr, name of chronological target system
#' @param chron_ref df chron_ref chronology description df
#'
#' @return num, matrix for the system conversion, from in rows and to in columns
#' @export
#'
#' @examples
chron_trans_matrix <- function(csys_from,
                               csys_to,
                               chron_ref = chron_ref){
    chron_ref_from <- chron_ref[chron_ref$c_system == csys_from,
                                c(7,9,10)]
    chron_ref_to <- chron_ref[chron_ref$c_system == csys_to,
                              c(7,9,10)]
    trans <- matrix(data = rep(0, length(chron_ref_from[,1]) * length(chron_ref_to[,1])),
                    nrow = length(chron_ref_from[,1]),
                    ncol = length(chron_ref_to[,1]))
    rownames(trans) <- chron_ref_from[,1]
    colnames(trans) <- chron_ref_to[,1]
    for (i in 1:length(chron_ref_from[,1])) {
        for (j in 1:length(chron_ref_to[,1])) {
            trans[i,j] <- intervall_prob(x1 = chron_ref_from$start[i],
                                         x2 = chron_ref_from$end[i],
                                         y1 = chron_ref_to$start[j],
                                         y2 = chron_ref_to$end[j])
        }
    }
    atlist <- attributes(trans)
    attributes(trans) <- c(atlist,
                           csys_from = csys_from,
                           csys_to   = csys_to)
    return(trans)
}
