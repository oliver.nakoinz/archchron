#' chron_trans
#'
#' Transforms a date in the chron_ref format to different chronological reference system using a transformation matrix. This kind of transformation usally includes a lack of information and precision.
#'
#' @param dat  df chron_ref date
#' @param csys_to chr, target chronological reference system of the transformation
#' @param trans_matrix num, transformation matrix as produced by chron_trans_matrix
#' @param chron_ref  df chron_ref chronology description df
#'
#' @return
#' @export
#'
#' @examples
chron_trans <- function(dat,
                        csys_to,
                        trans_matrix,
                        chron_ref = chron_ref){
    result  <- chron_ref[chron_ref$c_system == csys_to,
                         c(1,2,3,4,5,6,7,9,10)]
    csys_from <- dat[1,1]
    values <- dat$value
    trans <- trans_matrix * values
    values <- apply(trans,
                    MARGIN = 2,
                    FUN    = sum)
    result$value <- values
    return(result)
}
