#' chron_calc_count
#'
#' this function first turns the values into 1/0 Booleans (not T/F) and then calculates one function of "sum", "max", "min", "sum", "diff", "and", "or", "eq", or "not"
#'
#' @param dat1 chron_ref first date
#' @param dat2 chron_ref second date
#' @param thres num threshold: values above thres are turned to 1 else 0
#' @param fun chr function: "sum", "max", "min", "sum", "diff" (absolute), "and", "or", "eq", "not"
#'
#' @return chron_ref object
#' @export
#'
#' @examples
chron_calc_count <- function(dat1,
                             dat2,
                             thres = "0.5",
                             fun = c("sum", "max", "min", "diff", "and", "or", "eq", "not")){
    dat1[1,1] != dat2[1,1]
    if (dat1[1,1] != dat2[1,1]){
        stop("not the same chronology reference system")}
    dat3 <- dat1
    dat1$value <- ifelse(dat1$value >= thres, 1, 0)
    dat2$value <- ifelse(dat2$value >= thres, 1, 0)
    if (fun == "sum"){
        dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = "sum")} else if (fun == "max") {
            dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = "max")} else if (fun == "min") {
                dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = "min")} else if (fun == "sum") {
                    dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = "sum")} else if (fun == "diff") {
                        dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = function(a,b){abs(a-b)})} else if (fun == "and") {
                            dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = function(a,b){as.numeric(a & b)})} else if (fun == "or") {
                                dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = function(a,b){as.numeric(a | b)})} else if (fun == "eq") {
                                    dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = function(a,b){as.numeric(a == b)})} else if (fun == "not") {
                                        dat3$value <- apply(cbind(dat1$value, dat2$value), MARGIN = 1, FUN = function(a,b){as.numeric(a != b)})}
    return(dat3)
}
