#' chron_write_rds
#'
#' writes chronology data to rds file
#'
#' @param dates_list list dates list as produced by chron_process
#' @param data_stringlist list dates string list as produced by chron_string_list
#' @param type chr "string" = list with chronology strings, "chron_ref" = list with chron_ref dataframe, "both" = both
#' @param file chr filename (without ".rds") and path
#'
#' @return nothing
#' @export
#'
#' @examples
chron_write_rds <- function(dates_list,
                            data_stringlist,
                            type = c("both", "string", "chron_ref"),
                            file = "./chron"){
    if(type == "chron_ref"){
        saveRDS(dates_list, paste0(file, ".rds"))
    }
    if(type == "string"){
        saveRDS(data_stringlist, paste0(file, ".rds"))
    }
    if(type == "both"){
        saveRDS(list(chron_ref = dates_list,
                     string = data_stringlist),
                paste0(file,
                       ".rds"))
    }
}
