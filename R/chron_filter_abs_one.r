#' chron_filter_abs_one
#'
#' Tests, if an absolute date (y) with an uncertainty (+-a) has a probability of at least p to be included in the invervall of dat.
#'
#' @param dat   df chron_ref date
#' @param y num, year to test for
#' @param a num,  uncertainty (+-years)
#' @param p num, minimal probability
#'
#' @return log, true or false
#' @export
#'
#' @examples
chron_filter_abs_one <- function(dat,
                                 y,
                                 a = 50,
                                 p = 0.7) {
    y1 <- y - a
    y2 <- y + a
    val <- dat$value
    for (i in 1:dat$value) {
        val[i] <- dat$value[i] * intervall_prob(x1 = dat$start[i],
                                                x2 = dat$end[i],
                                                y1 = y1,
                                                y2 = y2)
    }
    if (max(val) >= p) {result  <- TRUE} else {
        result <- FALSE
    }
    return(result)
}
