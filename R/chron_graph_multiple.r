#' chron_graph_multiple
#'
#' #' produces a plot of multiple chron_refs
#'
#' @param dat list of chron_ref date
#' @param cmin start of plotted time interval
#' @param cmax end of plotted time interval
#' @param thres int if thres = 0 not considered, if thres > 0 used to turn values in occurences
#' @param main chr vector same length as dat, title of the graph, if NULL no title is plotted
#'
#' @return -
#' @export
#'
#' @examples
chron_graph_multiple <- function(dat,
                                 cmin  = - 1500,
                                 cmax  = 0,
                                 thres = 0,
                                 main  = NULL){
    nn <- length(dat)
    if(is.null(main)){main <- rep(NULL, nn)}

    par(mfrow=c((nn+1),1))
    for(i in 1:nn){
        chron_graph_single(dat[[i]],
                           cmin = cmin,
                           cmax = cmax,
                           thres = thres,
                           main  = main[i],
                           xaxt  = FALSE)
    }
    axis(1)
    par(mfrow=c(1,1))
}
