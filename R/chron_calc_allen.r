#' chron_calc_allen
#'
#' calculates the relationship of two time intervals according to Allen (1983): James F. Allen: Maintaining knowledge about temporal intervals. In: Communications of the ACM. 26/11/1983. ACM Press. S. 832–843
#'
#' @param dat1 df first date chron_ref format
#' @param dat2 df second date chron_ref format
#' @param hlevel int hierarchy level for the comparison or chronology string of the intended hierarchy level
#' @param thres num probability threshold
#' @param type chr short (Allen operator) or long (description) output
#'
#' @return chr short or long description
#' @export
#'
#' @examples
chron_calc_allen <- function(dat1,
                             dat2,
                             hlevel = 2,
                             thres = "0.5",
                             type = c("short", "long")){
    hlevel <- chron_hlevel(hlevel)
    dat1[1,1] != dat2[1,1]
    if (dat1[1,1] != dat2[1,1]){
        stop("not the same chronology reference system")}
    dat1$value <- ifelse(dat1$value >= thres, 1, 0)
    dat2$value <- ifelse(dat2$value >= thres, 1, 0)

    if (max(dat1$c_level[dat1$value == 1]) < hlevel | max(dat2$c_level[dat2$value == 1]) < hlevel){
        return(NA)
        stop("one or both dates do not reach the required chronology hierarchy level")}

    dat1$value[dat1$c_level != hlevel] <- 0
    start1 <- min(dat1$start[dat1$value == 1])
    end1   <- max(dat1$end[dat1$value == 1])
    dat2$value[dat2$c_level != hlevel] <- 0
    start2 <- min(dat2$start[dat2$value == 1])
    end2   <- max(dat2$end[dat2$value == 1])

    result <- allen(start1 = start1,
                    end1   = end1,
                    start2 = start2,
                    end2   = end2,
                    type   = if (type == "long"){"chronrel"} else {"chronrel_short"})

    return(result)
}
