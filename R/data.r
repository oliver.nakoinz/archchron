#' make_chron_ref
#'
#' prepares the basic chron_ref object from KFD for this package
#'
#' @param chron_ref_file
#'
#' @return -
#'
#' @examples
make_chron_ref <- function(chron_ref_file = NULL){
    if (is.null(chron_ref_file)){
        chron_ref_file <- R.utils::filePath("/home/fon/daten/db/befestigung_db/2data/22archdata/chronology.csv")} else {
            chron_ref_file <- R.utils::filePath(chron_ref_file)
        }
    chron_ref <- read.csv(chron_ref_file,
                          sep = ";",
                          stringsAsFactors = FALSE)
    chron_ref <- chron_ref[,1:10]  # exclude additional comments
    save(chron_ref, file="./data/chron_ref.RData")
}


#' make_fort_main_sites_selection
#'
#'prepares some example data from KFD, already available as csv in this folder for this package
#'
#' @param fort_main_sites
#'
#' @return
#'
#' @examples
make_fort_main_sites_selection <- function(fort_main_sites = NULL){
    if (is.null(fort_main_sites)){
        fort_main_sites <- R.utils::filePath("./data/fort_main_sites_selection.csv")} else {
            fort_main_sites <- R.utils::filePath(fort_main_sites)
        }
    fort_main_sites <- read.csv(fort_main_sites,
                          sep = ";",
                          stringsAsFactors = FALSE)
    save(fort_main_sites, file="data/fort_main_sites.RData")
}

# make_chron_ref()
# make_fort_main_sites_selection()
