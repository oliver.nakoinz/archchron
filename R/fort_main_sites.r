#' Fortification data
#'
#' This dataframe includes some data on selected fortifications, including chronology strings, from the Kiel Fortification Project
#'
#' @docType data
#'
#' @usage data(chron_ref)
#'
#' @format data.frame with 10 variables
#'
#'$ id_main     : int  fortification id (for internal usage only)
#'$ uuid        : chr  uuid site identification
#'$ uuid_parents: chr  superior sites id or "root"
#'$ name        : chr  site name
#'$ class       : chr  entry type, usually "site"
#'$ type        : chr  fortification type described with FDL (Fortification description language)
#'$ chron       : chr  chronology string such as "fortNE_Vorg>BZ>PerIV_1.0, fortNE_Vorg>BZ>PerV_1.0"
#'$ comment     : chr  any comments
#'$ source      : chr  literature and data sources
#'$ location_q  : int  quality of site location
#'$ chron_q     : int  quality of chronology
#'$ info_q      : int  general informatoin quality
#'$ WKT         : location in well known text format
#'
#' @keywords datasets
#'
#' @references NAKOINZ, Oliver, Jutta KNEISEL, Ines BEILKE-VOIGT and Jana DRÄGER, 2017. [Befestigungen der Bronze- und Eisenzeit zwischen Marburg und Uppsala](https://edition-topoi.org/articles/details/befestigungen-der-bronze--und-eisenzeit-zwischen-marburg-und-uppsala). In: Ines BEILKE-VOIGT and Oliver NAKOINZ, eds. Enge Nachbarn: Das Problem von Doppelburgen und Mehrfachburgen in der Bronzezeit und im Mittelalter. Berlin: Topoi, Edition Topoi, p. 21–88.
#'
#'
#' @source Kiel Fortification Project
#'
#' @examples
#' data(fort_main_sites)
#' head(fort_main_sites)
#'
