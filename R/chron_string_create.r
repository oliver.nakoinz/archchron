#' chron_string_create
#'
#' Create one chronstring from a chronstring oject
#'
#' @param chron_ref_line: df one line of a chron_ref object
#'
#' @return chr chronstring
#' @export
#'
#' @examples
chron_string_create <- function (chron_ref_line){
    chronstring <- paste0(chron_ref_line[6], "_", chron_ref_line[11])
    chronstring <- trimws(chronstring, "both")
    return(chronstring)
}
