#' chron_filter_string
#'
#' @param pattern chr, search pattern chronology string
#' @param object chr vector of chrontring sets
#' @param thres num probability threshold value, overrides the value from the pattern if thres > 0
#'
#' @return logical vector of colums which contain the pattern date, can be used to filter data
#' @export
#'
#' @examples
chron_filter_string <- function(pattern,
                                object,
                                thres = 0){
    pat <- chron_string_decompose(pattern)
    pat_sys    <- pat[[1]]
    pat_chron  <- paste(rev(pat[[2]]), collapse = ">")
    pat_prob   <- pat[[3]]
    if (thres == 0){pat_prob   <- pat[[3]]}else{
        pat_prob  <- thres}
    probs <- ceiling(10*pat_prob)
    steps <- (10:probs) / 10
    steps <- c(steps, pat_prob)
    pat_search <- paste0(pat_sys,
                         "_",
                         pat_chron,
                         "_",
                         steps)
    filter_m <- matrix(FALSE,
                       length(object),
                       length(pat_search))
    for (i in 1:length(pat_search)){
        filter_m[,i] <- grepl(pat_search[i],
                              object)
    }
    result <- as.logical(apply(filter_m,
                               MARGIN = 1,
                               FUN = "sum"))
    return(result)
}
